package br.edu.cest.pet.entity.arca;

public enum EnumPeso {
	QUILO("kg"),
	GRAMA("g");
	
	String val;
	
	private EnumPeso(String val) {
		this.val = val;
	}
}

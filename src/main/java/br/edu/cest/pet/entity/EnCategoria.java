package br.edu.cest.pet.entity;

public enum EnCategoria {
	ATEND(1), VET(2),
	ESTAGIARIO(3);
	
	public int value;
	
	private EnCategoria(int v) {
		this.value = v;
	}
}

package br.edu.cest.pet.entity.arca;

import br.edu.cest.pet.vo.EnumLocomocao;

public interface AnimalIFC {
	public String getNome();
	public String getFala();
	public String getEspecie();
	public EnumLocomocao getLocomocao();
	public EnumPeso getUnidadePeso();
	@Override
	public String toString();
}

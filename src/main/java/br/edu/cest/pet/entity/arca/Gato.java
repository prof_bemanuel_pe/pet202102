package br.edu.cest.pet.entity.arca;

public class Gato extends Felino {
	public Gato(String nome) {
		super(nome);
	}
	
	@Override
	public String getEspecie() {
		return "gato";
	}

	@Override
	public void limpeza() {
		System.out.
		   println("caixa de areia");
	}
}

package br.edu.cest.pet.entity.arca;

import br.edu.cest.pet.vo.EnumLocomocao;

public class Ave extends AnimalAbs {

	public Ave(String nome) {
		super(nome);
	}

	@Override
	public String getFala() {
		return "";
	}

	@Override
	public String getEspecie() {
		return "";
	}

	@Override
	public EnumLocomocao getLocomocao() {
		return EnumLocomocao.BIPEDEDE;
	}

	@Override
	public EnumPeso getUnidadePeso() {
		return EnumPeso.GRAMA;
	}

}

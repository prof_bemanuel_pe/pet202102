package br.edu.cest.pet.divisao;

import java.util.ArrayList;

import br.edu.cest.pet.entity.staff.Funcionario;

public class Cirurgica extends Setor {
	ArrayList<Funcionario> staff;
	@Override
	public String getDivisao() {
		return "Ala Cirurgica";
	}

	@Override
	public ArrayList<Funcionario> getStaff() {
		return staff;
	}


}

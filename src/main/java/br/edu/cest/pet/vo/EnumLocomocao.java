package br.edu.cest.pet.vo;

public enum EnumLocomocao {
	BIPEDEDE(2), 
	QUADRUPEDE(4);

	public int val;

	private EnumLocomocao(int val) {
		this.val = val;
	}
}
